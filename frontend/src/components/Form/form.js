import React from "react";

export const Form = () => {
  const formSubmit = (e) => {
    e.preventDefault()
    const data = {
      content: e?.target[0].value,
      from: e?.target[1].value,
      to: e?.target[2].value,
    }
    const url = "http://localhost:7080"
    const response = fetch(`${url}/send/sms`, {
    method: "POST",
    body: JSON.stringify(data),
  }).then((res) => console.log(res));

  console.log(response)

}

  return (
    <form onSubmit={(e) => formSubmit(e)} style={{width: "100%", height: "100vh", display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center", gap: "8px"}}>
      <input type="text" name="content" />
      <input type="text" name="from"/>
      <input type="text" name="to"/>
      <button type="submit">yuborish</button>
    </form>
  );
};