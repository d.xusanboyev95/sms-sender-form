package rest

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Status    string      `json:"status"`
	ErrorCode int         `json:"error_code"`
	ErrorNote string      `json:"error_note"`
	Data      interface{} `json:"data"`
}

type Result struct {
	Result bool `json:"result"`
}

//nolint:exhaustruct
func Return(c *gin.Context, data interface{}, err error) {
	switch {
	case err == nil:
		c.JSON(http.StatusOK, Response{
			Status:    "Success",
			ErrorNote: "",
			Data:      data,
		})

	default:
		c.JSON(http.StatusInternalServerError, Response{
			Status:    "Failure",
			ErrorNote: err.Error(),
			Data:      data,
		})
	}
}
